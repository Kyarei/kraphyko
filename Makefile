CC=tigcc
CFLAGS=-DKTI89 -O2 -Wall -Wextra -std=gnu99 --all-relocs -mregparm
LFLAGS=--optimize-code --remove-unused --native --cut-ranges --merge-constants --reorder-sections

SRCS=errcodes.c editor.c drawing.c kraphyko.c
OBJS=$(SRCS:.c=.o)

all: kraphyko.89z aaa.89z

kraphyko.89z: $(OBJS)
	@echo -e '\e[33mCompiling kraphyko.89z...\e[0m'
	@$(CC) $(LFLAGS) $^ -o $@
	@echo -e '\e[32mDone!\e[0m'

aaa.89z: aaa.c
	@echo -e '\e[33mCompiling aaa.89z...\e[0m'
	@$(CC) $(CFLAGS) $(LFLAGS) $^ -o $@
	@echo -e '\e[32mDone!\e[0m'

%.o: %.c %.d
	@echo -e '\e[33mCompiling $@...\e[0m'
	@$(CC) -c $(CFLAGS) $< -o $@

DEPS:=$(OBJS:.o=.d)

%.d: %.c
	@gcc -MM $< | \
		sed -e 's,^\([^:]*\)\.o[ ]*:,$(@D)/\1.o $(@D)/\1.d:,' > $@

-include $(DEPS)

clean:
	rm *.o *.d kraphyko.89z aaa.89z
