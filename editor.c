#include "editor.h"

#include <tigcclib.h>

#include "errcodes.h"

INT_HANDLER I1 /*= 0*/, I5 /*= 0*/;

void enableUndo(UndoInfo* p) {
  p->canUndo = allocGBUB(&(p->prevl), &(p->prevd));
}
void disableUndo(UndoInfo* p) {
  p->canUndo = 0;
  free(p->prevl);
  free(p->prevd);
}
void* allocBUB(void) {
  void* buffer = NULL;
  if (!(buffer = calloc(LCD_SIZE, 1))) {
    dlgError(DMA_ERROR, MEMALLOC_FAIL);
    return NULL;
  }
  return buffer;
}
int allocGBUB(void** lbuff, void** dbuff) {
  if (!(*lbuff = allocBUB())) { return 0; }
  if (!(*dbuff = allocBUB())) {
    free(*lbuff);
    return 0;
  }
  return 1;
}
void GLCDSave(void* lbuff, void* dbuff) {
  memcpy(lbuff, GrayGetPlane(LIGHT_PLANE), LCD_SIZE);
  memcpy(dbuff, GrayGetPlane(DARK_PLANE), LCD_SIZE);
}
void GLCDRestore(void* lbuff, void* dbuff) {
  memcpy(GrayGetPlane(LIGHT_PLANE), lbuff, LCD_SIZE);
  memcpy(GrayGetPlane(DARK_PLANE), dbuff, LCD_SIZE);
}
void beginDialog(void* lbuff, void* dbuff) {
  GLCDSave(lbuff, dbuff);
  GrayOff();
  SetIntVec(AUTO_INT_1, I1);
  SetIntVec(AUTO_INT_5, I5);
}
void endDialog(void* lbuff, void* dbuff) {
  SetIntVec(AUTO_INT_1, DUMMY_HANDLER);
  SetIntVec(AUTO_INT_5, DUMMY_HANDLER);
  GrayOn();
  GLCDRestore(lbuff, dbuff);
}
void undo(UndoInfo* p) {
  if (!p->isUndone && p->canUndo) {
    void *l, *d;
    allocGBUB(&l, &d);
    GLCDSave(l, d); // save current image
    GLCDRestore(p->prevl, p->prevd); // restore previous image
    memcpy(p->prevl, l, LCD_SIZE); // set undo fields to current image
    memcpy(p->prevd, d, LCD_SIZE);
    free(l);
    free(d);
    p->isUndone = 1;
  }
}
void redo(UndoInfo* p) {
  if (p->isUndone && p->canUndo) {
    void *l, *d;
    allocGBUB(&l, &d);
    GLCDSave(l, d); // save previous image
    GLCDRestore(p->prevl, p->prevd); // restore current image
    memcpy(p->prevl, l, LCD_SIZE); // set undo fields to previous image
    memcpy(p->prevd, d, LCD_SIZE);
    free(l);
    free(d);
    p->isUndone = 0;
  }
}
void refUndo(UndoInfo* p) {
  if (p->canUndo) {
    GLCDSave(p->prevl, p->prevd);
    p->isUndone = 0;
  }
}
void resetCB(ClipBoard* c) { // Resets clipboard (clears contents).
  if (c->lp) free(c->lp);
  if (c->dp) free(c->dp);
  c->lp = NULL;
  c->dp = NULL;
  c->w = 0;
  c->h = 0;
}
void copy(ClipBoard* c, const SCR_RECT* w) {
  resetCB(c); // we don't want anything in the clipboard
  BITMAP* b = NULL;
  long int size = BitmapSize(w); // size of bitmap
  b = malloc(size); // allocate memory
  if (!b) return; // error handling
  int i;
  for (i = 0; i < 2; i++) { // for both light and dark planes
    GraySetAMSPlane(i);
    BitmapGet(w, b); // get the bitmap
    void* a = malloc(size - BITMAP_HDR_SIZE); // allocate buffer for ClipBoard
                                              // structure to refer toi
    *(((void**) c + i)) = a; // if (i) c->lp=a; else c->dp=a;
    memcpy(a, b->Data, size - BITMAP_HDR_SIZE);
  }
  c->w = b->NumCols;
  c->h = b->NumRows;
  free(b);
}
