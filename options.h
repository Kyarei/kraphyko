#pragma once

#ifndef OPTIONS_H
#  define OPTIONS_H

#  ifdef KTI89
#    define USE_TI89
#  else // KTI89
#    define USE_TI92PLUS
#    define USE_V200
#  endif // KTI89

#endif // OPTIONS_H
