#include "errcodes.h"

const char* errors[] = {"DynMemAlloc Error", "Failed to allocate memory",
                        "File I/O Error",    "Unable to open file",
                        "File Saved",        "File was successfully saved",
                        "Dialog Error",      "Unable to set up dialog",
                        "Cannot draw text",  "String is out of bounds"};
