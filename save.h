#pragma once

#ifndef SAVE_H
#  define SAVE_H

typedef enum NameDialogRes {
  NDR_OK = 0,
  NDR_NO_HANDLE = 1,
  NDR_ABORTED = 2,
} NameDialogRes;

#endif // SAVE_H
