#pragma once
#ifndef KEYBOARD_H
#  define KEYBOARD_H

#  include "options.h"

#  include <tigcclib.h>

typedef unsigned short KbdMask;

#  define readArrows() _rowread(~0x0001)

#  ifdef KTI89
#    define isUp(k) (((k) &0x01) != 0)
#    define isDown(k) (((k) &0x04) != 0)
#    define isLeft(k) (((k) &0x02) != 0)
#    define isRight(k) (((k) &0x08) != 0)
#    define anyArrowsPressed(k) (((k) &0x0f) != 0)
#    define anyModifiersPressed(k) (((k) &0xf0) != 0)
#  else
#    define isUp(k) (((k) &0x20) != 0)
#    define isDown(k) (((k) &0x80) != 0)
#    define isLeft(k) (((k) &0x10) != 0)
#    define isRight(k) (((k) &0x40) != 0)
#    define anyArrowsPressed(k) (((k) &0xf0) != 0)
#    define anyModifiersPressed(k) (((k) &0x0f) != 0)
#  endif

inline int isEnterPressed() {
#  ifdef KTI89
  return (_rowread(~0x0002) & 0x01) != 0;
#  else
  return (_rowread(~0x0200) & 0x02) != 0 || // ENTER1
         (_rowread(~0x0040) & 0x40) != 0; // ENTER2
#  endif
}

inline int isEscPressed() {
#  ifdef KTI89
  return (_rowread(~0x0040) & 0x01) != 0;
#  else
  return (_rowread(~0x0100) & 0x40) != 0;
#  endif
}

inline int isEnterOrEscPressed() {
#  ifdef KTI89
  return (_rowread(~0x0042) & 0x01) != 0;
#  else
  return (_rowread(~0x0200) & 0x02) != 0 || // ENTER1
         (_rowread(~0x0140) & 0x40) != 0; // ENTER2 or ESC
#  endif
}

#endif // KEYBOARD_H
