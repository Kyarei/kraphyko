#pragma once
#ifndef DRAWING_H
#  define DRAWING_H

#  include <tigcclib.h>

enum Colors { WHITE, LGRAY, DGRAY, BLACK };
enum GAttrs {
  GA_DRAW, // replace color
  GA_ROT, // rotate: new color = (old color + brush color) % 4
  GA_FLIP, // flip:   new color = 3 - old color
  GA_ADD, // add:    new color = ???
};
enum Modes { M_NEW, M_OPEN };
enum DitherModes { D_NONE, D_RANDOM, D_3, D_4a, D_4b };
enum BrushModes {
  BR_PT,
  BR_R3,
  BR_R5,
  BR_R7,
  BR_HL3,
  BR_VL3,
  BR_PS3,
  BR_NS3,
  BR_HL5,
  BR_VL5,
  BR_PS5,
  BR_NS5,
  BR_D3,
  BR_D5
};
enum LineBrushOffsets { O_H, O_V, O_P, O_N };
#  define GA_NORMAL GA_DRAW
#  define GA_CYCLE GA_ROT
#  define OPTIONS 15
#  define CURSOR_DELAY 900

#  define picsize(w, h) (((w) * (h) + 3) / 4)

extern SCR_RECT whole_scn;

inline int sign(int x) { return (x > 0) ? 1 : ((x < 0) ? -1 : 0); }

void DrawStr2(int x, int y, const char* str, short Attr);
short GetGPix(short x, short y);
void DrawClipPixAttr(int x, int y, int attr);
void DrawGPix(short x, short y, short color, short attr);
void DrawGLine(short x0, short y0, short x1, short y1, short color, short attr);
void DrawGLine2(
    short x0, short y0, short x1, short y1, short color, short attr,
    char penWidth);
void DrawGTriangle(
    short x0, short y0, short x1, short y1, short x2, short y2, short color,
    short attr, char penWidth);
void DrawGTriangleFill(
    short x0, short y0, short x1, short y1, short x2, short y2, short color,
    short attr);
void DrawGRect(
    short x0, short y0, short x1, short y1, short color, short attr,
    char penWidth);
void DrawGRectFill(
    short x0, short y0, short x1, short y1, short color, short attr);
void DrawGEllipse(
    short x0, short y0, short x1, short y1, short color, short attr);
void DrawGEllipse2(
    short x0, short y0, short x1, short y1, short color, short attr,
    char penWidth);
void DrawGEllipseFill(
    short x0, short y0, short x1, short y1, short color, short attr);
void DrawGCirc(short x, short y, short r, short color, short attr);
void DrawGCirc2(
    short x, short y, short r, short color, short attr, char penWidth);
void DrawGCircFill(short x, short y, short r, short color, short attr);
void DrawGStr(short x0, short y0, const char* string, short color, short attr);
void FloodFill(short x, short y, short tcol, short rcol);

// This is implemented as a macro because declaring it as an inline function
// results in a larger executable.
#  define Fill(x, y, col) FloodFill(x, y, GetGPix(x, y), col)

#endif // DRAWING_H
