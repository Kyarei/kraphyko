#pragma once
#ifndef EDITOR_H
#  define EDITOR_H

#  include <tigcclib.h>

#  include "errcodes.h"

typedef struct {
  void *prevl, *prevd;
  unsigned int canUndo, isUndone;
} UndoInfo;
typedef struct {
  void *lp, *dp;
  unsigned int w, h;
} ClipBoard;

void enableUndo(UndoInfo* p);
void disableUndo(UndoInfo* p);
void* allocBUB(void);
int allocGBUB(void** lbuff, void** dbuff);
void GLCDSave(void* lbuff, void* dbuff);
void GLCDRestore(void* lbuff, void* dbuff);
void beginDialog(void* lbuff, void* dbuff);
void endDialog(void* lbuff, void* dbuff);
void undo(UndoInfo* p);
void redo(UndoInfo* p);
void refUndo(UndoInfo* p);
void resetCB(ClipBoard* c);
void copy(ClipBoard* c, const SCR_RECT* w);

extern INT_HANDLER I1, I5;

inline void setUpBUB(void* buffer) { PortSet(buffer, 239, 127); }
inline void fileSaved(void) { dlgError(FILE_SAVED, SAVE_SUCCESS); }

#endif
