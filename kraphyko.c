/*

post-dev comment:

This is an image editor for the TI-89 (Titanium) that I worked on from 2012 to
2013. This is its source as of May 4, 2013. (taken from
https://www.omnimaga.org/ti-68k-projects/kraphyko-decthyth-and-the-illusiat-12-port/15/)
Everything is included in one source file, since TIGCC / GCC4TI on Windows had a
bug with finding header files. This will likely still work if you create a new
GCC4TI project and apply the sprite routine fix
(https://github.com/debrouxl/gcc4ti/wiki/SprtRoutinesFix).
Don't listen to TI when they tell you not to run their AMSes on third-party
emulators. As indicated in the comment below, I started in 2012 (nearly 4 years
ago!)

*/

// C Source File
// Created 5/29/2012; 8:07:55 PM

/*
Specifiaction:
+------+----|-----------
|Offset|Size|Description
|  0x00|0x01|Width.
|  0x01|0x01|Height.
|  0x02|   N|Pixel data (0=white, 1=light gray, 2=dark gray, 3=black)

Brush types:
0 - point
1 - 3x3 rect
2 - 5x5 rect
3 - 7x7 rect
4 - horizontal line (len=3)
5 - vertical line
6 - positively sloping line
7 - negatively sloping line
8 - 11 - same as 4 - 7, but 5 px long
12 - 3x3 diamond
13 - 5x5 diamond
*/

#define SAVE_SCREEN

#include "options.h"

#include <tigcclib.h>

#include "drawing.h"
#include "editor.h"
#include "errcodes.h"
#include "keyboard.h"
#include "save.h"

// start
#define versString "Kraphyko 0.8_06 \200"
#define nameOfProgram "Kraphyko"
#define FNR "File name?"
char penWidth = 1;
UndoInfo ui = {NULL, NULL, 0, 0};
ClipBoard cb = {NULL, NULL, 0, 0};

#define floord2(x) ((x) / 2)

// Creates a delay.
void delay(int a) {
  short randNum;
  // generate random numbers to slow down the program...
  while (a-- > 0) { randNum = rand() % a; }
}

SCR_RECT NormalizeScrRect(SCR_RECT raw) {
  unsigned char x = raw.xy.x0, y = raw.xy.y0, a = raw.xy.x1, b = raw.xy.y1;
  return (SCR_RECT){{min(x, a), min(y, b), max(x, a), max(y, b)}};
}

void cut(ClipBoard* c, const SCR_RECT* w) {
  copy(c, w);
  DrawGRectFill(w->xy.x0, w->xy.y0, w->xy.x1, w->xy.y1, 0, GA_DRAW);
}
void paste(ClipBoard* c, unsigned char x, unsigned char y) {
  if (!c->w) return;
  BITMAP* b = NULL;
  int size = BITMAP_HDR_SIZE + c->w * c->h / 8;
  b = malloc(size);
  if (!b) return;
  b->NumRows = c->h;
  b->NumCols = c->w;
  int i, oldWd;
  oldWd = penWidth;
  penWidth = 1;
  for (i = 0; i < 2; i++) {
    GraySetAMSPlane(i);
    memcpy(b->Data, *(((void**) c + i)), size - 4);
    BitmapPut(x, y, b, &whole_scn, A_REPLACE);
  }
  penWidth = oldWd;
  free(b);
}

void ClrScrAll(void) {
  memset(GrayGetPlane(0), 0, LCD_SIZE);
  memset(GrayGetPlane(1), 0, LCD_SIZE);
}
void* GetMemLoc(SYM_STR name) // returns the memory location of the file
{
  return HeapDeref(DerefSym(SymFind(name))->handle);
}
// save as KPIC
short SaveKPIC(
    char* fname, unsigned int w, unsigned int h, void* lbuff, void* dbuff) {
  FILE* f = fopen(fname, "wb");
  if (!f) {
    beginDialog(lbuff, dbuff);
    dlgError(FILE_ERROR, FOPEN_FAIL);
    endDialog(lbuff, dbuff);
    return 0;
  }
  int i, bytes = picsize(w, h);
  unsigned int x = 0, y = 0, mm;
  fputc(w, f);
  fputc(h, f);
  for (i = 0; i < bytes; i++) {
    mm = 64 * GetGPix(x, y);
    x++;
    if (x >= w) {
      y++;
      x = 0;
    }
    mm += 16 * GetGPix(x, y);
    x++;
    if (x >= w) {
      y++;
      x = 0;
    }
    mm += 4 * GetGPix(x, y);
    x++;
    if (x >= w) {
      y++;
      x = 0;
    }
    fputc(mm + GetGPix(x, y), f);
    x++;
    if (x >= w) {
      y++;
      x = 0;
    }
  }
  fputc(0, f);
  fputs("KPIC", f);
  fputc(0, f);
  fputc(OTH_TAG, f);
  fclose(f);
  return 1;
}
short SavePIC(const char* fname, int dither) {
  /*dither ops*/
  if (dither) {
    int i, j, h, w;
    h = whole_scn.xy.y1;
    w = whole_scn.xy.x1;
    for (i = 0; i < h; i++) {
      for (j = 0; j < w; j++) {
        int col = GetGPix(j, i);
        switch (dither) {
        case D_RANDOM:
          DrawGPix(j, i, (random(3) < col) ? 3 : 0, GA_DRAW);
          break;
        case D_3:
          DrawGPix(
              j, i, ((col == 3) || (col && ((i + j) % 2))) ? 3 : 0, GA_DRAW);
          break;
        case D_4a:
          DrawGPix(j, i, (((i + j) % 3) <= col) ? 3 : 0, GA_DRAW);
          break;
        case D_4b: DrawGPix(j, i, (i % 3 <= col) ? 3 : 0, GA_DRAW); break;
        }
      }
    }
  }
  /*save*/
  GraySetAMSPlane(DARK_PLANE);
  char* buff = NULL;
  if (!(buff = malloc(BitmapSize(&whole_scn)))) return 0;
  BitmapGet(&whole_scn, buff);
  FILE* f = fopen(fname, "wb");
  fwrite(buff, BitmapSize(&whole_scn), 1, f);
  int i;
  for (i = 0; i < 9; i++) fputc(0, f);
  fputc(PIC_TAG, f);
  fclose(f);
  free(buff);
  return 1;
}
short show_picvar(
    SYM_STR SymName, short x, short y, short Attr, int* wl, int* hl) {
  SYM_ENTRY* sym_entry = SymFindPtr(SymName, 0);
  if (!sym_entry) return FALSE;
  if (peek(HToESI(sym_entry->handle)) != PIC_TAG) return FALSE;
  short* loc = HeapDeref(sym_entry->handle) + 2;
  BitmapPut(x, y, loc, ScrRect, Attr);
  *wl = *((short*) loc);
  *hl = *((short*) loc + 1);
  return TRUE;
}
void OpenPIC(const char* fname, int* wl, int* hl) {
  ClrScrAll();
  int i;
  for (i = 0; i < 2; i++) {
    GraySetAMSPlane(i);
    show_picvar(SYMSTR(fname), 0, 0, A_NORMAL, wl, hl);
  }
}
short OpenKPIC(const char* fname, int* wl, int* hl) {
  FILE* f = fopen(fname, "rb");
  if (!f) { return 0; }
  unsigned char w = fgetc(f);
  unsigned char h = fgetc(f);
  int i, bytes = picsize(w, h);
  int x = 0, y = 0, mm, col;
  for (i = 0; i < bytes; i++) {
    mm = fgetc(f);
    col = (mm & 0b11000000) >> 6;
    DrawGPix(x, y, col, GA_DRAW);
    x++;
    if (x >= w) {
      y++;
      x = 0;
    }
    col = (mm & 0b00110000) >> 4;
    DrawGPix(x, y, col, GA_DRAW);
    x++;
    if (x >= w) {
      y++;
      x = 0;
    }
    col = (mm & 0b00001100) >> 2;
    DrawGPix(x, y, col, GA_DRAW);
    x++;
    if (x >= w) {
      y++;
      x = 0;
    }
    col = mm & 0b00000011;
    DrawGPix(x, y, col, GA_DRAW);
    x++;
    if (x >= w) {
      y++;
      x = 0;
    }
  }
  fclose(f);
  *wl = w;
  *hl = h;
  return 1;
}
short OpenKPIC2(const char* fname, int* wl, int* hl) {
  char* f = GetMemLoc(SYMSTR(fname)) + 2;
  if (f == (char*) 2) { return 0; }
  unsigned char w = *(f++);
  unsigned char h = *(f++);
  int i, bytes = picsize(w, h);
  int x = 0, y = 0, col;
  unsigned char mm;
  for (i = 0; i < bytes; i++) {
    mm = *(f++);
    col = (mm & 0b11000000) >> 6;
    DrawGPix(x, y, col, GA_DRAW);
    x++;
    if (x >= w) {
      y++;
      x = 0;
    }
    col = (mm & 0b00110000) >> 4;
    DrawGPix(x, y, col, GA_DRAW);
    x++;
    if (x >= w) {
      y++;
      x = 0;
    }
    col = (mm & 0b00001100) >> 2;
    DrawGPix(x, y, col, GA_DRAW);
    x++;
    if (x >= w) {
      y++;
      x = 0;
    }
    col = mm & 0b00000011;
    DrawGPix(x, y, col, GA_DRAW);
    x++;
    if (x >= w) {
      y++;
      x = 0;
    }
  }
  *wl = w;
  *hl = h;
  return 1;
}
NameDialogRes
requestName(char* fname, short* typeID, int* dither, void* lbuff, void* dbuff) {
  beginDialog(lbuff, dbuff);
  int buff[] = {1, 0};
  HANDLE h = DialogNewSimple(150, 50);
  if (!h) {
    dlgError(DLG_ERROR, DLGSETUP_FAIL);
    return NDR_NO_HANDLE;
  }
  DialogAddTitle(h, nameOfProgram, BT_OK, BT_CANCEL);
  DialogAddRequest(h, 7, 12, "File name:", 0, 8, 8);
  HANDLE p = PopupNew("File Type", 0);
  if (!p) {
    dlgError(DLG_ERROR, DLGSETUP_FAIL);
    HeapFree(h);
    return NDR_NO_HANDLE;
  }
  PopupAddText(p, -1, "Kraphyko Pic (.KPIC)", 1);
  PopupAddText(p, -1, "TIOS Pic (.PIC)", 2);
  DialogAddPulldown(h, 7, 20, "File Type:", p, 0);
  HANDLE q = PopupNew("Dither (TI-OS Pics):", 0);
  if (!q) {
    dlgError(DLG_ERROR, DLGSETUP_FAIL);
    HeapFree(h);
    HeapFree(p);
    return NDR_NO_HANDLE;
  }
  PopupAddText(q, -1, "None", 1);
  PopupAddText(q, -1, "Random", 2);
  PopupAddText(q, -1, "3-color", 3);
  PopupAddText(q, -1, "Diagonals", 4);
  PopupAddText(q, -1, "Horizontal", 5);
  DialogAddPulldown(h, 7, 28, "Dither (TI-OS Pics):", q, 1);
  short key = DialogDo(h, CENTER, CENTER, fname, buff);
  HeapFree(h);
  HeapFree(p);
  HeapFree(q);
  *typeID = *(buff);
  *dither = buff[1] - 1;
  endDialog(lbuff, dbuff);
  return (key == KEY_ESC) ? NDR_ABORTED : NDR_OK;
}
const unsigned char cursorl[8] = {0x80, 0xC0, 0xA0, 0x90,
                                  0xB8, 0xE0, 0x90, 0x08};
const unsigned char cursord[8] = {0x80, 0xC0, 0xE0, 0xF0,
                                  0xF8, 0xE0, 0x90, 0x08};
const unsigned char cursorm[8] = {0x7F, 0x3F, 0x1F, 0x0F,
                                  0x07, 0x1F, 0x6F, 0xF7};
void drawCursor(unsigned int x, unsigned int y) // Draws the cursor
{
  Sprite8(x, y, 8, cursorm, GrayGetPlane(LIGHT_PLANE), SPRT_AND);
  Sprite8(x, y, 8, cursorm, GrayGetPlane(DARK_PLANE), SPRT_AND);
  Sprite8(x, y, 8, cursorl, GrayGetPlane(LIGHT_PLANE), SPRT_OR);
  Sprite8(x, y, 8, cursord, GrayGetPlane(DARK_PLANE), SPRT_OR);
}
int moveCursor(int* xp, int* yp, void* lbuff, void* dbuff, int w, int h) {
  GKeyFlush();
  int x = *xp, y = *yp;
  int k;
  int redraw = TRUE;
  do {
    if (redraw) {
      GLCDSave(lbuff, dbuff);
      drawCursor(x, y);
    }
    k = readArrows();
    int oldX = x, oldY = y;
    x = x - (isLeft(k) && x != 0) + (isRight(k) && x != w - 1);
    y = y - (isUp(k) && y != 0) + (isDown(k) && y != h - 1);
    redraw = (oldX != x) || (oldY != y);
    delay(CURSOR_DELAY);
    k = isEnterPressed() ? 1 : isEscPressed() ? 2 : 0;
    if (redraw || k) GLCDRestore(lbuff, dbuff);
  } while (!k);
  *xp = x;
  *yp = y;
  delay(1500);
  while (!isEnterOrEscPressed()) {}
  return k == 1;
}
char brushstyle = 0;
void Pencil(int* xp, int* yp, void* lbuff, void* dbuff, int w, int h, int col) {
  GKeyFlush();
  int x = *xp, y = *yp;
  int k;
  int stuck = FALSE;
  int redraw = TRUE;
  GLCDSave(lbuff, dbuff);
  do {
    if (redraw) { drawCursor(x, y); }
    k = readArrows();
    int oldX = x, oldY = y;
    x = x - (isLeft(k) && x != 0) + (isRight(k) && x != w - 1);
    y = y - (isUp(k) && y != 0) + (isDown(k) && y != h - 1);
    int moved = oldX != x || oldY != y;
    redraw = moved;
    if (moved) stuck = FALSE;
    delay(CURSOR_DELAY);
    if (anyModifiersPressed(k) && !stuck) {
      GLCDRestore(lbuff, dbuff);
      int attr = GA_NORMAL + !!(k & 96) + !!(k & 64);
      int mat = attr == GA_ROT ? GA_DRAW : attr;
      switch (brushstyle) {
      case 0: DrawGPix(x, y, col, attr); break;
      case 1:
      case 2:
      case 3:
        DrawGRect(
            x - brushstyle, y - brushstyle, x + brushstyle, y + brushstyle, col,
            mat, penWidth);
        break;
      case 12: DrawGPix(x, y - 1, col, mat); DrawGPix(x, y + 1, col, mat);
      case 4: DrawGLine(x - 1, y, x + 1, y, col, mat); break;
      case 5: DrawGLine(x, y - 1, x, y + 1, col, mat); break;
      case 6: DrawGLine(x - 1, y + 1, x + 1, y - 1, col, mat); break;
      case 7: DrawGLine(x - 1, y - 1, x + 1, y + 1, col, mat); break;
      case 13:
        DrawGLine(x - 1, y - 1, x + 1, y - 1, col, mat);
        DrawGLine(x - 1, y + 1, x + 1, y + 1, col, mat);
        DrawGPix(x, y - 2, col, mat);
        DrawGPix(x, y + 2, col, mat);
      case 8: DrawGLine(x - 2, y, x + 2, y, col, mat); break;
      case 9: DrawGLine(x, y - 2, x, y + 2, col, mat); break;
      case 10: DrawGLine(x - 2, y + 2, x + 2, y - 2, col, mat); break;
      case 11: DrawGLine(x - 2, y - 2, x + 2, y + 2, col, mat); break;
      }
      stuck = TRUE;
      redraw = TRUE;
      GLCDSave(lbuff, dbuff);
    } else {
      k = _rowread(~0x0042);
      if (k & 128) {
        brushstyle = (brushstyle + 1) % 12;
        // GLCDSave(lbuff, dbuff);
        char buff[14];
        sprintf(buff, "Brush style %d", brushstyle);
        int temp = FontSetSys(F_4x6);
        DrawGStr(2, 92, buff, 3, GA_DRAW);
        FontSetSys(temp);
        delay(3 * CURSOR_DELAY);
        GLCDRestore(lbuff, dbuff);
        redraw = TRUE;
      }
    }
    k = isEnterOrEscPressed();
    if (redraw || k) GLCDRestore(lbuff, dbuff);
  } while (!k);
}
const char* attrStrings[4] = {"Normal", "Rotate", "Flip", "Add"};
const char* yesOrNo[2] = {"No", "Yes"};
const char* fontSizes[3] = {"Small", "Normal", "Large"};
char* a = "1";
void updateSettings(
    int* col, int* attr, int* fontsize, int* filled, void* lbuff, void* dbuff,
    int* w, int* h) // char* canUndo,char* lineThickness
{
  GLCDSave(lbuff, dbuff);
  int i, n = 0, k;
  for (i = 0; i < 2; i++) {
    GraySetAMSPlane(i);
    ClrScr();
    FontSetSys(F_6x8);
    DrawStr(0, 0, versString, A_NORMAL);
    DrawLine(0, 8, 159, 8, A_NORMAL);
    FontSetSys(F_4x6);
    DrawStr(5, 10, "Color:", A_NORMAL);
    DrawStr(5, 16, "Attribute:", A_NORMAL);
    DrawStr(5, 22, "Filled:", A_NORMAL);
    DrawStr(5, 28, "Font Size:", A_NORMAL);
    DrawStr(5, 34, "Resize...", A_NORMAL);
    DrawStr(5, 40, "Pen Width:", A_NORMAL);
    DrawStr(5, 46, "Enable Undo:", A_NORMAL);
    DrawStr(39, 16, attrStrings[*attr], A_XOR);
    DrawStr(28, 22, yesOrNo[*filled], A_XOR);
    DrawStr(39, 28, fontSizes[*fontsize], A_XOR);
    DrawStr(48, 46, yesOrNo[ui.canUndo], A_XOR);
    DrawStr(40, 40, a, A_XOR);
  }
  DrawGRectFill(24, 10, 30, 15, *col, GA_DRAW);
  DrawChar(1, 10, '>', A_XOR);
  do {
    k = _rowread(~0x0001);
    DrawChar(1, 10 + 6 * n, '>', A_XOR);
    n = n - (k & 1) + !!(k & 4);
    if (n == -1) n = 6;
    if (n == 7) n = 0;
    DrawChar(1, 10 + 6 * n, '>', A_XOR);
    if (k & 8) {
      switch (n) {
      case 0:
        *col = (*col + 1) % 4;
        DrawGRectFill(24, 10, 30, 15, *col, GA_DRAW);
        break;
      case 1:
        DrawGStr(39, 16, attrStrings[*attr], 3, GA_FLIP);
        *attr = (*attr + 1) % 4;
        DrawGStr(39, 16, attrStrings[*attr], 3, GA_FLIP);
        break;
      case 2:
        DrawGStr(28, 22, yesOrNo[*filled], 3, GA_FLIP);
        *filled = !(*filled);
        DrawGStr(28, 22, yesOrNo[*filled], 3, GA_FLIP);
        break;
      case 3:
        DrawGStr(39, 28, fontSizes[*fontsize], 3, GA_FLIP);
        *fontsize = (*fontsize + 1) % 3;
        DrawGStr(39, 28, fontSizes[*fontsize], 3, GA_FLIP);
        break;
      case 4:
        GrayOff();
        SetIntVec(AUTO_INT_1, I1);
        SetIntVec(AUTO_INT_5, I5);
        HANDLE hn = DialogNewSimple(140, 60);
        if (hn != H_NULL) {
          char ReqBuff[8];
          sprintf(ReqBuff, "%d", *w);
          sprintf(ReqBuff + 4, "%d", *h);
          DialogAddTitle(hn, nameOfProgram, BT_OK, BT_CANCEL);
          DialogAddRequest(hn, 7, 12, "New width:", 0, 3, 3);
          DialogAddRequest(hn, 7, 20, "New height:", 4, 3, 3);
          if ((DialogDo(hn, CENTER, CENTER, ReqBuff, NULL)) == KEY_ENTER) {
            int a = atoi(ReqBuff);
            *w = a > 0 && a <= 160 ? a : *w;
            a = atoi(ReqBuff + 4);
            *h = a > 0 && a <= 100 ? a : *h;
            whole_scn.xy.x1 = *w - 1;
            whole_scn.xy.y1 = *h - 1;
          }
        } else {
          dlgError(DLG_ERROR, DLGSETUP_FAIL);
        }
        HeapFree(hn);
        SetIntVec(AUTO_INT_1, DUMMY_HANDLER);
        SetIntVec(AUTO_INT_5, DUMMY_HANDLER);
        GrayOn();
        break;
      case 5:
        DrawGStr(40, 40, a, 3, GA_FLIP);
        penWidth = (penWidth % 9) + 1;
        *a = '0' + penWidth;
        DrawGStr(40, 40, a, 3, GA_FLIP);
        break;
      case 6:
        DrawGStr(48, 46, yesOrNo[ui.canUndo], 3, GA_FLIP);
        if (ui.canUndo)
          disableUndo(&ui);
        else
          enableUndo(&ui);
        memcpy(GrayGetPlane(LIGHT_PLANE), GrayGetPlane(DARK_PLANE), LCD_SIZE);
        GraySetAMSPlane(LIGHT_PLANE);
        DrawChar(1, 10 + 6 * n, '>', A_XOR);
        GraySetAMSPlane(DARK_PLANE);
        DrawGStr(48, 46, yesOrNo[ui.canUndo], 3, GA_FLIP);
      }
    }
    delay(2700);
    k = _rowread(~0x0002);
  } while (!(k & 1));
  GLCDRestore(lbuff, dbuff);
}
const char* OptionTips[48] = {"Save (3)",
                              "Save As (+)",
                              "Quit (Esc)",
                              0,
                              0,
                              0,
                              0,
                              0,
                              0,
                              0,
                              0,
                              0,
                              0,
                              0,
                              0,
                              0,
                              "Undo (Z)",
                              "Redo (Y)",
                              "Cut (X)",
                              "Copy (=)",
                              "Paste (0)",
                              "Options (-)",
                              0,
                              0,
                              0,
                              0,
                              0,
                              0,
                              0,
                              0,
                              0,
                              0,
                              "Pencil (\026)",
                              "Line (4)",
                              "Rectangle (2)",
                              "Circle ())",
                              "Ellipse (/)",
                              "Triangle (T)",
                              "Text (*)",
                              "Fill (|)",
                              "Invert (9)",
                              "Clear",
                              "Polygon (^)",
                              0,
                              0,
                              0,
                              0,
                              0};
char options[3] = {3, 6, 11};
void DrawMS(char Tab, char* buff, char* fname) {
  int i;
  for (i = 0; i < 2; i++) {
    GraySetAMSPlane(i);
    ClrScr();
    FontSetSys(F_6x8);
    DrawStr(0, 0, versString, A_NORMAL);
    DrawLine(0, 8, 159, 8, A_NORMAL);
    FontSetSys(F_4x6);
    DrawStr(5, 10, "F1 - Help | F2 - File | F3 - Edit | F4 - Image", A_NORMAL);
    int j;
    const char** b = OptionTips + 16 * Tab;
    for (j = 0; j < 16; j++, b++) {
      const char* a = *b;
      DrawStr(5, 16 + 6 * j, a, A_NORMAL);
    }
    DrawStr(100, 20, fname, A_NORMAL);
    DrawStr(100, 26, buff, A_NORMAL);
  }
}
int MenuScn(void* lbuff, void* dbuff, int w, int h, char* fname) {
  GLCDSave(lbuff, dbuff);
  char buff[20];
  sprintf(buff, "%dX%d", w, h);
  int Tab = 0; // this variable shows which tab is open
  int k, n = 0;
  DrawMS(Tab, buff, fname);
  DrawChar(1, 16, '>', A_NORMAL);
  do {
    k = _rowread(~0x0001);
    DrawChar(1, 16 + 6 * n, '>', A_XOR);
    n = n - (k & 1) + !!(k & 4);
    if (n == -1) n = options[Tab] - 1;
    if (n == options[Tab]) n = 0;
    DrawChar(1, 16 + 6 * n, '>', A_XOR);
    delay(2700);
    k = _rowread(~0x0004);
    if (k & 2) {
      Tab = 0;
      n = 0;
      break;
    } // save
    if (k & 8) {
      Tab = 2;
      n = 8;
      break;
    } // invert
    if (k & 32) {
      Tab = 2;
      n = 5;
      break;
    } // triangle
    if (k & 128) {
      Tab = 2;
      if (n >= options[2]) n = options[2] - 1;
      DrawMS(Tab, buff, fname);
      DrawChar(1, 16 + 6 * n, '>', A_NORMAL);
    }
    k = _rowread(~0x0008);
    if (k & 2) {
      Tab = 2;
      n = 2;
      break;
    } // rectangle
    // if (k&8) {n=14; break;} //help now is accessed by F1
    if (k & 16) {
      Tab = 2;
      n = 3;
      break;
    } // circle
    if (k & 128) {
      Tab = 1;
      if (n >= options[1]) n = options[1] - 1;
      DrawMS(Tab, buff, fname);
      DrawChar(1, 16 + 6 * n, '>', A_NORMAL);
    }
    k = _rowread(~0x0010);
    if (k & 4) {
      Tab = 2;
      n = 1;
      break;
    } // line
    if (k & 128) {
      Tab = 0;
      if (n >= *options) n = *options - 1;
      DrawMS(Tab, buff, fname);
      DrawChar(1, 16 + 6 * n, '>', A_NORMAL);
    }
    k = _rowread(~0x0020);
    if (k & 2) {
      Tab = 2;
      n = 0;
      break;
    } // pencil
    if (k & 8) {
      Tab = 2;
      n = 7;
      break;
    } // fill
    if (k & 32) {
      Tab = 1;
      n = 2;
      break;
    } // cut
    if (k & 128) {
      Tab = 3;
      n = 0;
      break;
    } // show help
    if (_rowread(~0x0040)) {
      Tab = 0;
      n = 2;
      break;
    } // quit
    k = _rowread(~0x0002);
    if (k & 2) {
      Tab = 0;
      n = 1;
      break;
    } // save as
    if (k & 4) {
      Tab = 1;
      n = 5;
      break;
    } // options
    if (k & 8) {
      Tab = 2;
      n = 6;
      break;
    } // text
    if (k & 16) {
      Tab = 2;
      n = 4;
      break;
    } // ellipse
    if (k & 64) {
      Tab = 2;
      n = 9;
      break;
    } // clear
  } while (!(k & 1));
  GLCDRestore(lbuff, dbuff);
  return 16 * Tab + n;
}
/*
Help File Format

2 bytes: number of items
4*(number of items) bytes: jump table, with (title,body) pairs
rest: help contents, using null-terminated strings

Left pane should contain the TOC in small font; right pane should have the title
in normal font and body in small font.
*/

void DrawWrapText(
    int x, int y, char* s, int attr, int wrapWidth, int maxHeight) {
  int font = FontGetSys();
  int i;
  int charHt = 2 * font + 6;
  int maxRows = maxHeight / charHt;
  int len = strlen(s);
  for (i = 0; (i < maxRows) && (len); i++) {
    int j = 0;
    char b, c = 1;
    while (c) {
      b = s[j];
      s[j] = 0;
      c = (DrawStrWidth(s, font) <= wrapWidth) && (j < len);
      s[j] = b;
      if (c)
        j++;
      else
        j--;
    }
    b = s[j];
    s[j] = 0;
    DrawStr(x, y + charHt * i, s, attr);
    s[j] = b;
    s += j;
    len -= j;
  }
}
// items on left pane shown
void UpdateHelpScn(
    int noOfItems, int* jumpTable, char* start, int active, int top,
    SCR_RECT rect) {
  ClrScr();
  DrawLine(60, 0, 60, 159, A_NORMAL);
  int i;
  for (i = 0; i < 16; i++) {
    if ((top + i) < noOfItems)
      DrawStr(0, 6 * i, start + jumpTable[2 * (top + i)], A_NORMAL);
  }
  rect.xy.y1 = 5 + (rect.xy.y0 = 6 * (active - top));
  ScrRectFill(&rect, &whole_scn, A_XOR);
  FontSetSys(F_6x8);
  DrawStr(62, 2, start + jumpTable[2 * (active)], A_NORMAL);
  FontSetSys(F_4x6);
  DrawWrapText(62, 11, start + jumpTable[2 * (active) + 1], A_NORMAL, 96, 86);
}

void help(void* lbuff, void* dbuff) {
  GLCDSave(lbuff, dbuff);
  GrayOff();
  SCR_RECT save = whole_scn;
  whole_scn = (SCR_RECT){{0, 0, 239, 127}};
  // initialize ^^^
  int noOfItems;
  int* jumpTable;
  void* location;
  char* start;
  int active = 0;
  int top = 0;
  int flag = 0; // this is the update flag
  location = GetMemLoc(SYMSTR("khelpf")) + 2;
  noOfItems = *(int*) location;
  jumpTable = (int*) location + 1;
  start = (char*) (jumpTable + 2 * noOfItems);
  SCR_RECT rect = {{0, 0, 59, 5}};
  int key = 0, esc = 0;
  FontSetSys(F_4x6);
  UpdateHelpScn(noOfItems, jumpTable, start, active, top, rect);
  while (1) {
    while (!((key = _rowread(~0x0001)) || (esc = _rowread(~0x0040))))
      ;
    if (esc & 1) break;
    if ((key & 1) && active) active--, flag = 1;
    if ((key & 4) && (active < (noOfItems - 1))) active++, flag = 1;
    if (active < top) top--;
    if (active >= (top + 16)) top++;
    if (flag) UpdateHelpScn(noOfItems, jumpTable, start, active, top, rect);
  }
  // finalize
  GrayOn();
  GLCDRestore(lbuff, dbuff);
  whole_scn = save;
}

void EditImg(
    int mode, char* fname, void* lbuff, void* dbuff, int w, int h,
    short int ftype) {
  ClrScrAll();
  whole_scn.xy.x0 = whole_scn.xy.y0 = 0;
  whole_scn.xy.x1 = w - 1;
  whole_scn.xy.y1 = h - 1;
  SetCurClip(&whole_scn);
  if (mode == M_OPEN) {
    if (ftype == 1)
      OpenKPIC2(fname, &w, &h);
    else
      OpenPIC(fname, &w, &h);
  }
  int done = 0, n;
  int x = 0, y = 0, col = 3, a = 0, b = 0, c = 0, d = 0, attr = 0, fontsize = 1,
      filled = 0, r = 0, dither = 0;
  char shadow[9];
  while (!done) {
    n = MenuScn(lbuff, dbuff, w, h, fname);
    delay(3000);
    done = n == 2;
    switch (n) {
    case 32:
      refUndo(&ui);
      Pencil(&x, &y, lbuff, dbuff, w, h, col);
      break;
    case 33:
      if (!moveCursor(&a, &b, lbuff, dbuff, w, h)) break;
      x = a, y = b;
      if (!moveCursor(&x, &y, lbuff, dbuff, w, h)) break;
      refUndo(&ui);
      DrawGLine2(x, y, a, b, col, attr, penWidth);
      break;
    case 34:
      if (!moveCursor(&a, &b, lbuff, dbuff, w, h)) break;
      x = a, y = b;
      if (!moveCursor(&x, &y, lbuff, dbuff, w, h)) break;
      refUndo(&ui);
      if (filled)
        DrawGRectFill(x, y, a, b, col, attr);
      else
        DrawGRect(x, y, a, b, col, attr, penWidth);
      break;
    case 35:
      if (!moveCursor(&x, &y, lbuff, dbuff, w, h)) break;
      a = x, b = y;
      if (!moveCursor(&a, &b, lbuff, dbuff, w, h)) break;
      long r2 = ((long) (a - x)) * (a - x) + ((long) (b - y)) * (b - y);
      r = sqrt(r2);
      refUndo(&ui);
      if (filled)
        DrawGCircFill(x, y, r, col, attr);
      else
        DrawGCirc2(x, y, r, col, attr, penWidth);
      break;
    case 36:
      if (!moveCursor(&a, &b, lbuff, dbuff, w, h)) break;
      x = a, y = b;
      if (!moveCursor(&x, &y, lbuff, dbuff, w, h)) break;
      refUndo(&ui);
      if (filled)
        DrawGEllipseFill(x, y, a, b, col, attr);
      else
        DrawGEllipse2(x, y, a, b, col, attr, penWidth);
      break;
    case 37:
      if (!moveCursor(&a, &b, lbuff, dbuff, w, h)) break;
      c = a, d = b;
      if (!moveCursor(&c, &d, lbuff, dbuff, w, h)) break;
      x = c, y = d;
      if (!moveCursor(&x, &y, lbuff, dbuff, w, h)) break;
      refUndo(&ui);
      if (filled)
        DrawGTriangleFill(x, y, a, b, c, d, col, attr);
      else
        DrawGTriangle(x, y, a, b, c, d, col, attr, penWidth);
      break;
    case 38:
      if (!moveCursor(&x, &y, lbuff, dbuff, w, h)) break;
      beginDialog(lbuff, dbuff);
      r = 1;
      HANDLE hn = H_NULL;
      char* buff = NULL;
      if (!(buff = malloc(100))) break;
      if (!(hn = DialogNewSimple(150, 90))) {
        free(buff);
        break;
      }
      DialogAddTitle(hn, nameOfProgram, BT_OK, BT_NONE);
      DialogAddRequest(hn, 7, 12, "String to Draw:", 0, 99, 20);
      DialogDo(hn, CENTER, CENTER, buff, NULL);
      HeapFree(hn);
      FontSetSys(fontsize);
      if ((x + DrawStrWidth(buff, fontsize)) > w) {
        dlgError(DRAWTXT_ERROR, STR_OOB);
        r = 0;
      }
      endDialog(lbuff, dbuff);
      refUndo(&ui);
      if (r) DrawGStr(x, y, buff, col, attr);
      free(buff);
      break;
    case 21:
      updateSettings(&col, &attr, &fontsize, &filled, lbuff, dbuff, &w, &h);
      break;
    case 39:
      if (!moveCursor(&x, &y, lbuff, dbuff, w, h)) break;
      refUndo(&ui);
      Fill(x, y, col);
      break;
    case 40:
      refUndo(&ui);
      GraySetAMSPlane(0), ScrRectFill(&whole_scn, &whole_scn, A_XOR);
      GraySetAMSPlane(1), ScrRectFill(&whole_scn, &whole_scn, A_XOR);
      break;
    case 41:
      refUndo(&ui);
      ClrScrAll();
      break;
    case 1:
      strcpy(shadow, fname);
      NameDialogRes res = requestName(fname, &ftype, &dither, lbuff, dbuff);
      if (res != NDR_OK) break;
      // fall through
    case 0:
      if (ftype == 1)
        SaveKPIC(fname, w, h, lbuff, dbuff);
      else {
        SavePIC(fname, dither);
        strcpy(fname, shadow);
      }
      *shadow = 0;
      break;
    case 48: help(lbuff, dbuff); break;
    case 16: undo(&ui); break;
    case 17: redo(&ui); break;
    case 18:
      if (!moveCursor(&a, &b, lbuff, dbuff, w, h)) break;
      x = a, y = b;
      if (!moveCursor(&x, &y, lbuff, dbuff, w, h)) break;
      SCR_RECT s01 = NormalizeScrRect((SCR_RECT){{x, y, a, b}});
      cut(&cb, &s01);
      break;
    case 19:
      if (!moveCursor(&a, &b, lbuff, dbuff, w, h)) break;
      x = a, y = b;
      if (!moveCursor(&x, &y, lbuff, dbuff, w, h)) break;
      SCR_RECT s02 = NormalizeScrRect((SCR_RECT){{x, y, a, b}});
      copy(&cb, &s02);
      break;
    case 20:
      if (!moveCursor(&x, &y, lbuff, dbuff, w, h)) break;
      paste(&cb, x, y);
      break;
    }
    delay(1500);
  }
}
int titleScn(void) {
  ClrScrAll();
  whole_scn.xy.x1 = 159;
  whole_scn.xy.y1 = 99;
  DrawGRectFill(0, 0, 159, 19, 1, GA_DRAW);
  FontSetSys(F_8x10);
  DrawGStr(5, 2, versString, 3, GA_DRAW);
  FontSetSys(F_4x6);
  DrawGStr(2, 92, "\251 2012 - 2018 Ardtha Productions", 3, GA_DRAW);
  FontSetSys(F_6x8);
  DrawGStr(12, 22, "New", 3, GA_DRAW);
  DrawGStr(12, 30, "Open", 3, GA_DRAW);
  DrawGStr(12, 38, "Quit", 3, GA_DRAW);
  GraySetAMSPlane(DARK_PLANE);
  int n = 0;
  KbdMask k;
  int countdown = 0;
  DrawChar(4, 22, 127, A_XOR);
  do {
    k = readArrows();
    int oldN = n;
    if (countdown == 0) n = n - isUp(k) + isDown(k);
    if (anyArrowsPressed(k) && countdown == 0)
      countdown = 27;
    else if (!anyArrowsPressed(k))
      countdown = 0;
    else
      --countdown;
    if (n == -1) n = 2;
    if (n == 3) n = 0;
    if (n != oldN) {
      DrawChar(4, 22 + 8 * oldN, 127, A_XOR);
      DrawChar(4, 22 + 8 * n, 127, A_XOR);
    }
    k = isEnterPressed();
    delay(200);
  } while (!k);
  while (isEnterPressed()) {} // Wait to release
  return n;
}

// Returns 0 (NDR_OK) on success, something else on failure
NameDialogRes
requestName2(char* fname, short* w, short* ht, void* lbuff, void* dbuff) {
  beginDialog(lbuff, dbuff);
  char buff[17];
  strcpy(buff, fname);
  sprintf(buff + 9, "%d", *w);
  sprintf(buff + 13, "%d", *ht);
  HANDLE h = DialogNewSimple(140, 50);
  if (!h) {
    dlgError(DLG_ERROR, DLGSETUP_FAIL);
    return NDR_NO_HANDLE;
  }
  DialogAddTitle(h, nameOfProgram, BT_OK, BT_CANCEL);
  DialogAddRequest(h, 7, 12, FNR, 0, 8, 8);
  DialogAddRequest(h, 7, 20, "Width:", 9, 3, 3);
  DialogAddRequest(h, 7, 28, "Height:", 13, 3, 3);
  short k = DialogDo(h, CENTER, CENTER, buff, NULL);
  HeapFree(h);
  strcpy(buff, fname);
  *w = atoi(buff + 9);
  *ht = atoi(buff + 13);
  endDialog(lbuff, dbuff);
  return (k == KEY_ESC) ? NDR_ABORTED : NDR_OK;
}
NameDialogRes requestName3(char* fname, void* lbuff, void* dbuff) {
  beginDialog(lbuff, dbuff);
  HANDLE h = DialogNewSimple(140, 40);
  if (!h) {
    dlgError(DLG_ERROR, DLGSETUP_FAIL);
    return NDR_NO_HANDLE;
  }
  DialogAddTitle(h, nameOfProgram, BT_OK, BT_CANCEL);
  DialogAddRequest(h, 7, 12, FNR, 0, 8, 8);
  short k = DialogDo(h, CENTER, CENTER, fname, NULL);
  HeapFree(h);
  endDialog(lbuff, dbuff);
  return (k == KEY_ESC) ? NDR_ABORTED : NDR_OK;
}
// end

// Main Function
void _main(void) {
  I1 = GetIntVec(AUTO_INT_1);
  I5 = GetIntVec(AUTO_INT_5);
  SetIntVec(AUTO_INT_1, DUMMY_HANDLER);
  SetIntVec(AUTO_INT_5, DUMMY_HANDLER);
  char fname[9] = "sample";
  void *lbuff = NULL, *dbuff = NULL;
  if (!allocGBUB(&lbuff, &dbuff)) goto end;
  short w = 64, h = 64;
  if (GrayOn()) {
    int n;
    while ((n = titleScn()) != 2) {
      int c = 1;
      NameDialogRes res;
      switch (n) {
      case 1:
        res = requestName3(fname, lbuff, dbuff);
        if (res != NDR_OK) goto back_to_title;
        HSym a = SymFind(SYMSTR(fname));
        if (a.folder) {
          SYM_ENTRY* b = DerefSym(a);
          int type = peek(HToESI(b->handle));
          if (!(type == PIC_TAG || type == OTH_TAG)) n = 2;
          c = 1 + (type == PIC_TAG);
        } else
          n = 2;
        if (n == 1) break;
      case 2:
        GrayOff();
        SetIntVec(AUTO_INT_1, I1);
        SetIntVec(AUTO_INT_5, I5);
        int shouldCreate =
            DlgMessage("File not found", "Create new KPIC?", BT_YES, BT_NO) ==
            KEY_ENTER;
        SetIntVec(AUTO_INT_1, DUMMY_HANDLER);
        SetIntVec(AUTO_INT_5, DUMMY_HANDLER);
        GrayOn();
        if (!shouldCreate) goto back_to_title;
        n = 0;
      case 0:
        res = requestName2(fname, &w, &h, lbuff, dbuff);
        if (res != NDR_OK) goto back_to_title;
      }
      EditImg(n, fname, lbuff, dbuff, w, h, c);
    back_to_title:;
    }
  }
end:
  GKeyFlush();
  GrayOff();
  free(lbuff);
  free(dbuff);
  if (ui.canUndo) disableUndo(&ui);
  resetCB(&cb);
  SetIntVec(AUTO_INT_1, I1);
  SetIntVec(AUTO_INT_5, I5);
}
