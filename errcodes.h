#pragma once
#ifndef ERRCODES_H
#  define ERRCODES_H

#  include <tigcclib.h>

enum ErrCodes {
  DMA_ERROR,
  MEMALLOC_FAIL,
  FILE_ERROR,
  FOPEN_FAIL,
  FILE_SAVED,
  SAVE_SUCCESS,
  DLG_ERROR,
  DLGSETUP_FAIL,
  DRAWTXT_ERROR,
  STR_OOB
};
extern const char* errors[];

#  define dlgError(title, body) \
    DlgMessage(errors[title], errors[body], BT_OK, BT_NONE)

#endif // ERRCODES_H
