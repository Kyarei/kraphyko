#include "drawing.h"

#include <tigcclib.h>

SCR_RECT whole_scn;

double sqr(double x) { return x * x; }

void DrawStr2(int x, int y, const char* str, short Attr) {
  if (Attr != A_REVERSE) {
    DrawStr(x, y, str, Attr);
  } else {
    ScrRectFill(&whole_scn, &whole_scn, A_XOR);
    DrawStr(x, y, str, A_NORMAL);
    ScrRectFill(&whole_scn, &whole_scn, A_XOR);
  }
}
short GetGPix(short x, short y) {
  GraySetAMSPlane(LIGHT_PLANE);
  int color = GetPix(x, y);
  GraySetAMSPlane(DARK_PLANE);
  return color + 2 * GetPix(x, y);
}
void DrawClipPixAttr(int x, int y, int attr) {
  SetCurAttr(attr);
  DrawClipPix(x, y);
}
void DrawGPix(short x, short y, short color, short attr) {
  int col;
  if (attr) col = GetGPix(x, y);
  switch (attr) {
  case GA_DRAW:
    GraySetAMSPlane(LIGHT_PLANE);
    DrawClipPixAttr(x, y, (color & 1));
    GraySetAMSPlane(DARK_PLANE);
    DrawClipPixAttr(x, y, (color & 2) / 2);
    break;
  case GA_ROT: DrawGPix(x, y, (col + color) % 4, GA_DRAW); break;
  case GA_FLIP: DrawGPix(x, y, 3 - col, GA_DRAW);
  }
}

static uint32_t leftPixelsSet(int number) {
  if (number == 0) return 0;
  // Return a bitmask that results in the left `number` pixels being set.
  uint32_t mask = (1UL << (32 - number)) - 1;
  return ~mask;
}

static void DrawLineHorizontalDraw(
    int mode, short rxl, short rx0, short rx1, uint32_t lmask, uint32_t rmask,
    uint32_t* mem) {
  if (mode) {
    if (rx1 != rxl) {
      memset(mem + rx0, 0xFF, sizeof(uint32_t) * (rx1 - rx0));
      mem[rxl] |= lmask;
      mem[rx1] |= rmask;
    } else {
      mem[rxl] |= lmask & rmask;
    }
  } else {
    if (rx1 != rxl) {
      memset(mem + rx0, 0x00, sizeof(uint32_t) * (rx1 - rx0));
      mem[rxl] &= ~lmask;
      mem[rx1] &= ~rmask;
    } else {
      mem[rxl] &= ~(lmask & rmask);
    }
  }
}

static void DrawLineHorizontalInv(
    short rxl, short rx0, short rx1, uint32_t lmask, uint32_t rmask,
    uint32_t* mem) {
  mem[rxl] ^= lmask;
  mem[rx1] ^= rmask;
  for (int col = rx0; col < rx1; ++col) { mem[col] = ~mem[col]; }
}

static void
rotateColorsByBitplanes(uint32_t* l, uint32_t* d, uint32_t mask, int times) {
  uint32_t light = *l, dark = *d;
  // Bits not set in mask are unaffected
  uint32_t bs[4] = {
      ~light & ~dark & mask,
      light & ~dark & mask,
      ~light & dark & mask,
      light & dark & mask,
  };
  // Colors 1 and 3 set the bit here; 0 and 2 reset
  uint32_t newlight =
      bs[(1 + 4 - times) & 3] | bs[(3 + 4 - times) & 3] | (~mask & light);
  // Colors 2 and 3 set the bit here; 0 and 1 reset
  uint32_t newdark =
      bs[(2 + 4 - times) & 3] | bs[(3 + 4 - times) & 3] | (~mask & dark);
  *l = newlight;
  *d = newdark;
}

void DrawGLineHorizontal(short x0, short x1, short y, short color, short attr) {
  if (y < 0 || y >= whole_scn.xy.y1) return;
  if (x0 < 0) x0 = 0;
  if (x0 > whole_scn.xy.x1) x0 = whole_scn.xy.x1;
  if (x1 < 0) x1 = 0;
  if (x1 > whole_scn.xy.x1) x1 = whole_scn.xy.x1;
  ++x1; // originally [x0, x1], now [x0, x1)
  int pixno0 = 240 * y + x0, pixno1 = 240 * y + x1;
  int rxl = pixno0 >> 5;
  int rx0 = (pixno0 + 31) >> 5;
  int rx1 = pixno1 >> 5;
  int firstLeftBit = pixno0 & 31;
  int lastRightBit = pixno1 & 31;
  uint32_t* lightRow = (uint32_t*) GrayGetPlane(LIGHT_PLANE);
  uint32_t* darkRow = (uint32_t*) GrayGetPlane(DARK_PLANE);
  int lightActive = (color & 1) != 0;
  int darkActive = (color & 2) != 0;
  uint32_t lmask = ~leftPixelsSet(firstLeftBit);
  uint32_t rmask = leftPixelsSet(lastRightBit);
  switch (attr) {
  case GA_DRAW:
    DrawLineHorizontalDraw(lightActive, rxl, rx0, rx1, lmask, rmask, lightRow);
    DrawLineHorizontalDraw(darkActive, rxl, rx0, rx1, lmask, rmask, darkRow);
    break;
  case GA_ROT:
  case GA_ADD: {
    for (short col = rx0; col < rx1; ++col) {
      rotateColorsByBitplanes(lightRow + col, darkRow + col, -1UL, attr);
    }
    if (rxl != rx1) {
      rotateColorsByBitplanes(lightRow + rxl, darkRow + rxl, lmask, attr);
      rotateColorsByBitplanes(lightRow + rx1, darkRow + rx1, rmask, attr);
    } else {
      rotateColorsByBitplanes(
          lightRow + rxl, darkRow + rxl, lmask & rmask, attr);
    }
    break;
  }
  case GA_FLIP:
    DrawLineHorizontalInv(rxl, rx0, rx1, lmask, rmask, lightRow);
    DrawLineHorizontalInv(rxl, rx0, rx1, lmask, rmask, darkRow);
  default: break;
  }
}

static void
lineShallow(short x0, short y0, short x1, short y1, short color, short attr) {
  int dx = x1 - x0;
  int dy = y1 - y0;
  int yi = 1;
  if (dy < 0) {
    dy = -dy;
    yi = -1;
  }
  int d = 2 * dy - dx;
  int y = y0;
  for (int x = x0; x <= x1; ++x) {
    if (x >= 0 && y >= 0 && y < whole_scn.xy.y1 && x < whole_scn.xy.x1)
      DrawGPix(x, y, color, attr);
    if (d > 0) {
      y += yi;
      d -= 2 * dx;
    }
    d += 2 * dy;
  }
}

static void
lineSteep(short x0, short y0, short x1, short y1, short color, short attr) {
  int dx = x1 - x0;
  int dy = y1 - y0;
  int xi = 1;
  if (dx < 0) {
    dx = -dx;
    xi = -1;
  }
  int d = 2 * dx - dy;
  int x = x0;
  for (int y = y0; y <= y1; ++y) {
    if (x >= 0 && y >= 0 && y < whole_scn.xy.y1 && x < whole_scn.xy.x1)
      DrawGPix(x, y, color, attr);
    if (d > 0) {
      x += xi;
      d -= 2 * dy;
    }
    d += 2 * dx;
  }
}

/*
  Kraphyko 0.8_05 and earlier used floating-point math to draw lines with
  the GA_ROT attribute. Later versions like this one use Bresenham's
  line drawing algorithm for all attributes
  (https://en.wikipedia.org/wiki/Bresenham%27s_line_algorithm).
*/
void DrawGLine(
    short x0, short y0, short x1, short y1, short color, short attr) {
  if (abs(y1 - y0) < abs(x1 - x0)) {
    if (x0 > x1)
      lineShallow(x1, y1, x0, y0, color, attr);
    else
      lineShallow(x0, y0, x1, y1, color, attr);
  } else {
    if (y0 > y1)
      lineSteep(x1, y1, x0, y0, color, attr);
    else
      lineSteep(x0, y0, x1, y1, color, attr);
  }
}

#define floord2(x) ((x) / 2)

void DrawGLine2(
    short x0, short y0, short x1, short y1, short color, short attr,
    char penWidth) {
  if (y0 == y1) {
    int it2;
    for (it2 = -(penWidth - 1); it2 <= (penWidth - 1); it2 += 2) {
      DrawGLine(x0, y0 + it2 / 2, x1, y1 + it2 / 2, color, attr);
    }
    return;
  }
  float mi = -(float) (x1 - x0) / ((float) (y1 - y0));
  float x = 1.0 / (sqrt(1.0 + sqr(mi)));
  float y = x * mi;
  int it2;
  for (it2 = -(penWidth - 1); it2 <= (penWidth - 1); it2 += 2) {
    DrawGLine(
        x0 + floord2(it2 * x), y0 + floord2(it2 * y), x1 + floord2(it2 * x),
        y1 + floord2(it2 * y), color, attr);
  }
}

void DrawGTriangle(
    short x0, short y0, short x1, short y1, short x2, short y2, short color,
    short attr, char penWidth) {
  DrawGLine2(x0, y0, x1, y1, color, attr, penWidth);
  DrawGLine2(x2, y2, x1, y1, color, attr, penWidth);
  DrawGLine2(x0, y0, x2, y2, color, attr, penWidth);
}

void DrawGTriangleFill(
    short x0, short y0, short x1, short y1, short x2, short y2, short color,
    short attr) {
  switch (attr) {
  case GA_DRAW:
    GraySetAMSPlane(LIGHT_PLANE);
    FillTriangle(x0, y0, x1, y1, x2, y2, &whole_scn, (color & 1));
    GraySetAMSPlane(DARK_PLANE);
    FillTriangle(x0, y0, x1, y1, x2, y2, &whole_scn, (color & 2) / 2);
    break;
  case GA_ROT:
  case GA_ADD:
    GraySetAMSPlane(LIGHT_PLANE);
    if (color & 1) FillTriangle(x0, y0, x1, y1, x2, y2, &whole_scn, A_NORMAL);
    GraySetAMSPlane(DARK_PLANE);
    if (color & 2) FillTriangle(x0, y0, x1, y1, x2, y2, &whole_scn, A_NORMAL);
    break;
  case GA_FLIP:
    GraySetAMSPlane(LIGHT_PLANE);
    FillTriangle(x0, y0, x1, y1, x2, y2, &whole_scn, A_XOR);
    GraySetAMSPlane(DARK_PLANE);
    FillTriangle(x0, y0, x1, y1, x2, y2, &whole_scn, A_XOR);
    break;
  }
}

void DrawGRect(
    short x0, short y0, short x1, short y1, short color, short attr,
    char penWidth) {
  DrawGLine2(x0, y0, x1, y0, color, attr, penWidth);
  DrawGLine2(x0, y0, x0, y1, color, attr, penWidth);
  DrawGLine2(x1, y0, x1, y1, color, attr, penWidth);
  DrawGLine2(x0, y1, x1, y1, color, attr, penWidth);
}

void DrawGRectFill(
    short x0, short y0, short x1, short y1, short color, short attr) {
  if (x1 < x0) {
    short t = x1;
    x1 = x0;
    x0 = t;
  }
  if (y1 < y0) {
    short t = y1;
    y1 = y0;
    y0 = t;
  }
  for (int y = y0; y <= y1; y++) {
    DrawGLineHorizontal(x0, x1, y, color, attr);
  }
}

static void DrawGEllipse3(
    short xc, short yc, short a, short b, short color, short attr,
    short intrusion) {
  long a2 = ((long) a) * a;
  long b2 = ((long) b) * b;
  long fa2 = 4 * a2;
  long fb2 = 4 * b2;
  int ai = a - intrusion;
  int bi = b - intrusion;
  if (ai < 0) ai = 0;
  if (bi < 0) bi = 0;
  long ai2 = ai * ai;
  long bi2 = bi * bi;
  long fai2 = 4 * ai2;
  long fbi2 = 4 * bi2;
  // Part 1
  int x = 0;
  int y = b;
  int yi = bi;
  long sigma = 2 * b2 + a2 * (1 - 2 * b);
  long sigmai = 2 * bi2 + ai2 * (1 - 2 * bi);
  while (b2 * x < a2 * y) {
    int ystart = yi;
    if (ystart < 0) ystart = 0;
    for (int i = ystart; i <= y; ++i) {
      DrawGPix(xc + x, yc + i, color, attr);
      if (x != 0) DrawGPix(xc - x, yc + i, color, attr);
      if (i != 0) {
        DrawGPix(xc + x, yc - i, color, attr);
        if (x != 0) DrawGPix(xc - x, yc - i, color, attr);
      }
    }
    if (sigma >= 0) {
      sigma += fa2 * (1 - y);
      --y;
    }
    if (sigmai >= 0) {
      sigmai += fai2 * (1 - yi);
      --yi;
    }
    sigma += b2 * ((4 * x) + 6);
    sigmai += bi2 * ((4 * x) + 6);
    ++x;
  }
  // Part 2
  int xlim = x;
  x = a;
  int xi = ai;
  y = 0;
  sigma = 2 * a2 + b2 * (1 - 2 * a);
  sigmai = 2 * bi2 + ai2 * (1 - 2 * bi);
  while (a2 * y < b2 * x) {
    int xstart = xi;
    if (xstart < xlim) xstart = xlim;
    if (x > xstart) {
      DrawGLineHorizontal(xc + xstart, xc + x, yc + y, color, attr);
      if (y != 0) DrawGLineHorizontal(xc + xstart, xc + x, yc - y, color, attr);
      if (xstart == 0) ++xstart;
      DrawGLineHorizontal(xc - x, xc - xstart, yc + y, color, attr);
      if (y != 0) DrawGLineHorizontal(xc - x, xc - xstart, yc - y, color, attr);
    }
    if (sigma >= 0) {
      sigma += fb2 * (1 - x);
      --x;
    }
    if (sigmai >= 0) {
      sigmai += fbi2 * (1 - xi);
      --xi;
    }
    sigma += a2 * ((4 * y) + 6);
    sigmai += ai2 * ((4 * y) + 6);
    ++y;
  }
}

/*
  Old versions of Kraphyko just called DrawClipEllipse. This code instead uses
  Bresenham's ellipse-drawing algorithm
  (see
  https://sites.google.com/site/ruslancray/lab/projects/bresenhamscircleellipsedrawingalgorithm/bresenham-s-circle-ellipse-drawing-algorithm).
*/
void DrawGEllipse(
    short x0, short y0, short x1, short y1, short color, short attr) {
  int xc = (x0 + x1) / 2;
  int yc = (y0 + y1) / 2;
  int a = abs(x1 - xc);
  int b = abs(y1 - yc);
  long a2 = ((long) a) * a;
  long b2 = ((long) b) * b;
  long fa2 = 4 * a2;
  long fb2 = 4 * b2;
  // Part 1
  int x = 0;
  int y = b;
  long sigma = 2 * b2 + a2 * (1 - 2 * b);
  while (b2 * x < a2 * y) {
    DrawGPix(xc + x, yc + y, color, attr);
    DrawGPix(xc - x, yc + y, color, attr);
    DrawGPix(xc + x, yc - y, color, attr);
    DrawGPix(xc - x, yc - y, color, attr);
    if (sigma >= 0) {
      sigma += fa2 * (1 - y);
      --y;
    }
    sigma += b2 * ((4 * x) + 6);
    ++x;
  }
  // Part 2
  x = a;
  y = 0;
  sigma = 2 * a2 + b2 * (1 - 2 * a);
  while (a2 * y < b2 * x) {
    DrawGPix(xc + x, yc + y, color, attr);
    DrawGPix(xc - x, yc + y, color, attr);
    DrawGPix(xc + x, yc - y, color, attr);
    DrawGPix(xc - x, yc - y, color, attr);
    if (sigma >= 0) {
      sigma += fb2 * (1 - x);
      --x;
    }
    sigma += a2 * ((4 * y) + 6);
    ++y;
  }
}
void DrawGEllipse2(
    short x0, short y0, short x1, short y1, short color, short attr,
    char penWidth) {
  if (penWidth == 1) DrawGEllipse(x0, y0, x1, y1, color, attr);
  int xd = abs(x1 - x0);
  int yd = abs(y1 - y0);
  DrawGEllipse3(
      (x0 + x1) / 2, (y0 + y1) / 2, xd / 2, yd / 2, color, attr,
      2 * (penWidth - 1));
}
void DrawGEllipseFill(
    short x0, short y0, short x1, short y1, short color, short attr) {
  int xd = abs(x1 - x0);
  int yd = abs(y1 - y0);
  DrawGEllipse3((x0 + x1) / 2, (y0 + y1) / 2, xd / 2, yd / 2, color, attr, 255);
}
void DrawGCirc(short x, short y, short r, short color, short attr) {
  DrawGEllipse(x - r, y - r, x + r, y + r, color, attr);
}
void DrawGCirc2(
    short x, short y, short r, short color, short attr, char penWidth) {
  DrawGEllipse2(x - r, y - r, x + r, y + r, color, attr, penWidth);
}
void DrawGCircFill(short x, short y, short r, short color, short attr) {
  DrawGEllipseFill(x - r, y - r, x + r, y + r, color, attr);
}

void DrawGStr(short x0, short y0, const char* string, short color, short attr) {
  switch (attr) {
  case GA_DRAW:
    GraySetAMSPlane(LIGHT_PLANE);
    DrawStr2(x0, y0, string, (color & 1));
    GraySetAMSPlane(DARK_PLANE);
    DrawStr2(x0, y0, string, (color & 2) >> 1);
    break;
  case GA_ROT:
    GraySetAMSPlane(LIGHT_PLANE);
    if (color & 1) DrawStr2(x0, y0, string, A_NORMAL);
    GraySetAMSPlane(DARK_PLANE);
    if (color & 2) DrawStr2(x0, y0, string, A_NORMAL);
    break;
  case GA_FLIP:
    GraySetAMSPlane(LIGHT_PLANE);
    DrawStr2(x0, y0, string, A_XOR);
    GraySetAMSPlane(DARK_PLANE);
    DrawStr2(x0, y0, string, A_XOR);
  }
}

static int ptcolora(int x, int y, short rcol) {
  return (x >= 0 && x <= whole_scn.xy.x1 && y >= 0 && y <= whole_scn.xy.y1) ?
             GetGPix(x, y) :
             rcol;
}

typedef struct {
  unsigned char x, y;
} aaa;

void FloodFill(short x, short y, short tcol, short rcol) {
  if (tcol == rcol) return;
  if (ptcolora(x, y, rcol) != tcol) return;
  long int size = 1;
  long int capacity = 16;
  HANDLE sh = HeapAlloc(capacity * sizeof(aaa));
  if (sh == H_NULL) return;
  aaa* stack = HeapDeref(sh);
  stack[0] = (aaa){x, y};
  int j, Y;
  while (size > 0) {
    aaa N = stack[size - 1];
    --size;
    Y = N.y;
    if (ptcolora(N.x, Y, rcol) == tcol) {
      int e = N.x;
      int w = e;
      do { --w; } while ((ptcolora(w, Y, rcol) == tcol) && w >= 0);
      do {
        ++e;
      } while ((ptcolora(e, Y, rcol) == tcol) && e <= whole_scn.xy.x1);
      ++w;
      --e;
      DrawGLineHorizontal(w, e, Y, rcol, GA_DRAW);
      for (j = w; j <= e; j++) {
        if (ptcolora(j, Y - 1, rcol) == tcol) {
          if (size >= capacity) {
            HeapUnlock(sh);
            sh = HeapRealloc(sh, 2 * capacity * sizeof(aaa));
            if (sh == H_NULL) goto ERROR;
            stack = HeapDeref(sh);
            capacity *= 2;
          }
          stack[size] = (aaa){j, Y - 1};
          ++size;
        }
        if (ptcolora(j, Y + 1, rcol) == tcol) {
          if (size >= capacity) {
            HeapUnlock(sh);
            sh = HeapRealloc(sh, 2 * capacity * sizeof(aaa));
            if (sh == H_NULL) goto ERROR2;
            stack = HeapDeref(sh);
            capacity *= 2;
          }
          stack[size] = (aaa){j, Y + 1};
          ++size;
        }
      }
    }
  }
  HeapFree(sh);
  return;
ERROR:
  HeapFree(sh);
  FloodFill(j, Y - 1, tcol, rcol);
  return;
ERROR2:
  HeapFree(sh);
  FloodFill(j, Y + 1, tcol, rcol);
  return;
}
